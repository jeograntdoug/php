<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Decument</title>
</head>
<body>
<?php
	function displayForm($name = '',$age = ''){
		$form=<<<ENDFORM
		<form>
			<div>
				<label>Name:</label>
				<input type="text" name="name" value="$name"/>
			</div>
			<div>
				<label>Age :</label>
				<input type="text" name="age" value="$age"/>
			</div>
			<input type="submit" value="Append to file">
		</form>
		ENDFORM;

		echo $form;
	}

	function addPersonToFile($name,$age){
		$file = 'people.txt';
		$handle = fopen($file,'a+');
		
		fwrite($handle, "$name;$age\n");
		fclose($handle);
	}


	if(isset($_GET['name'])){
		$name = $_GET['name'];
		$age = $_GET['age'];
		$errors = [];
		
		if(strlen($name) < 2 || strlen($name) >20 ){
			$errors['name'] = "Name must be 2~20 chars";
			$name = '';
		}

		if(!is_numeric($age) || $age < 1 || $age > 150 ){
			$errors['age'] = "Age must be 1~150";
			$age = '';
		}
		
		if(empty($errors))
		{	// STATE 2 : Submittion succeed

			addPersonTofile($name,$age);
			
			echo "Hello, $name. You are $age y/o!";
		} 
		else 
		{	// STATE 3 : Submittion failed
			$errorStr = "<ul>";
			foreach($errors as $error){
				$errorStr .= "<li>$error</li>";
			}
			$errorStr .= "</ul>";

			echo $errorStr;
			displayForm($name,$age);
		}
	} else {	// STATE 1 : no submittion
		displayForm();
	}
?>
</body>
</html>
