<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Slim\Exception\HttpNotFoundException;

require __DIR__ . '/vendor/autoload.php';

session_start();

$app = AppFactory::create();

// Define Custom Error Handler
$customErrorHandler = function (
    Psr\Http\Message\ServerRequestInterface $request,
    \Throwable $exception,
    bool $displayErrorDetails,
    bool $logErrors,
    bool $logErrorDetails
) use ($app) {
    $response = $app->getResponseFactory()->createResponse();
    // seems the followin can be replaced by your custom response
    // $page = new Alvaro\Pages\Error($c);
    // return $page->notFound404($request, $response);

    return $response->withHeader('Location', '/forbidden', 403);
};

// Add Error Middleware
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
// Register the handler to handle only  HttpNotFoundException
// Changing the first parameter registers the error handler for other types of exceptions
$errorMiddleware->setErrorHandler(Slim\Exception\HttpNotFoundException::class, $customErrorHandler);


// create a log channel

$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'quiz1slimshout';
DB::$password = '7WgrMTbkMNTkH3gk';
DB::$dbName = 'quiz1slimshout';
DB::$port = 3333;
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params)
{
    header("Location: /error_internal", 500);

    global $log;
    $log->error("Database erorr[Connection]: " . $params['error']);

    if ($params['query']) {
        $log->error("Database error[Query]: " . $params['query']);
    }
    die();
}

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache', 'debug' => true]);

// Set Global variable($_SESSION)
$twig->getEnvironment()->addGlobal('session', $_SESSION);


// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));


$app->get('/forbidden', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_internal', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'index.html.twig');
});

$app->get('/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    return $view->render($response, 'login.html.twig');
});

$app->post('/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $loginInfo = $request->getParsedBody();

    if ($loginInfo != null) {
        if (isset($loginInfo['username']) && isset($loginInfo['password'])) {
            $user = DB::queryFirstRow("SELECT * FROM users WHERE username = %s", $loginInfo['username']);
            if ($loginInfo['password'] === $user['password']) {
                unset($user['password']);
                $_SESSION['user'] = $user;

                return $response
                    ->withHeader('Location', '/');
            }
        }
    }

    return $view->render($response, 'login.html.twig', [
        'error' => "Username doesn't match password."
    ]);
});

$app->get('/logout', function (Request $request, Response $response) {
    unset($_SESSION['user']);
    return $response->withHeader('Location', '/');
});

$app->get('/register', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});

$app->post('/register', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $registerInfo = $request->getParsedBody();

    $username = $registerInfo['username'];
    $password = $registerInfo['password'];
    $photo = $_FILES['photo'];

    $errors = [];

    if (strlen($username) < 6 || strlen($username) > 20) {
        $errors['username'] = "Username must be 6~20 chars";
        $registerInfo['username'] = '';
    }elseif( preg_match("/^[a-zA-Z0-9_]+$/",$username) == false){
        $errors['username'] = "Username must be charactors and underscore";
        $registerInfo['username'] = '';
    } elseif (isUsernameTaken($username)) {
        $errors['username'] = "User is already exist.";
        $username = '';
    }

    $ext = '';
    if ($ext = isValidPhoto($photo)) {
        $photoPath = "uploads/$username.$ext";
    } else {
        $errors['photo'] = "Invalid photo!";
    }


    if (
        strlen($password) < 5 || strlen($password) > 100
        || preg_match("/[a-z]/", $password) == false
        || preg_match("/[A-Z]/", $password) == false
        || preg_match("/[0-9#$%^&*()+=-\[\]';,.\/{}|:<>?~]/", $password) == false
    ) {
        $errors['password'] = "Password must be 5~100 characters,
                            must contain at least one uppercase letter, 
                            one lower case letter, 
                            and one number or special character.";
    } elseif ($password !== $_POST['confirm']) {
        $errors['password'] = "Passwords must be same.";
    }

    if (empty($errors)) {
        move_uploaded_file($photo['tmp_name'], $photoPath);

        DB::insert('users', [
            'username' => $username,
            'imagePath' => '/' . $photoPath,
            'password' => $password
        ]);
        $user = DB::affectedRows()[0];

        unset($user['password']);

        $_SESSION['user'] = $user;

        return $response->withHeader('Location', '/');
    }

    return $view->render($response, 'register.html.twig', [
        'errors' => $errors,
        'prevInput' => [
            'username' => $username,
        ]
    ]);
});


$app->get('/shouts/list', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    $shouts= DB::query( 
        "SELECT u.username AS 'author', u.imagePath AS 'imagePath',
            s.message AS 'message'
        FROM shouts AS s
        JOIN users AS u
        ON u.id = s.authorId"
    );

    $authorList = [];
    foreach($shouts as $shout){
        if(!in_array($shout['author'], $authorList)){
            array_push($authorList, $shout['author']);
        }
    }

    return $view->render($response, "shouts.html.twig", [
        'shouts' => $shouts,
        'authorList' => $authorList,
    ]);
});



$app->get('/shouts/add', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (!isset($_SESSION['user'])) {
        //TODO : show user a message says "must login first"
        return $response->withHeader('Location', '/login');
    }

    return $view->render($response, "shoutadd.html.twig");
});

$app->post('/shouts/add', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (!isset($_SESSION['user'])) {
        //TODO : show user massege days "Must login first"
        return $response->withHeader('Location', '/login');
    }

    $articleInfo = $request->getParsedBody();
    $message = $articleInfo['message'];
    $errors = [];

    //Sanitize the body
    //1) only allow certain HTML tags, 2) make sure it is valid HTMl
    $message = strip_tags($message, "<p><ul><li><em><strong><i><b><ol><h3><h4><span>");

    if (empty($message) || strlen($message) > 100) {
        $errors['message'] = "Message must be 1~100 characters.";
    }

    if (empty($errors)) {
        DB::insert('shouts', [
            'message' => $message,
            'authorId' => $_SESSION['user']['id']
        ]);
        return $view->render($response, "shoutadd.html.twig",[
            'submit' => 'succeed'
        ]);
    }

    return $view->render($response, 'shoutadd.html.twig', [
        'errors' => $errors,
        'prevInput' => [
            'message' => $message
        ]
    ]);
});


$app->get('/ajax/loadshouts[/by/{username}]',function (Request $request, Response $response, array $args) {

    $shouts = [];
    if(!isset($args['username'])){
        $shouts= DB::query( 
            "SELECT u.username AS 'author', u.imagePath AS 'imagePath',
                s.message AS 'message'
            FROM shouts AS s
            JOIN users AS u
            ON u.id = s.authorId"
        );

    } else {
        $username = $args['username'];
        $shouts= DB::query( 
            "SELECT u.username AS 'author', u.imagePath AS 'imagePath',
                s.message AS 'message'
            FROM shouts AS s
            JOIN users AS u
            ON u.id = s.authorId
            WHERE u.username = %s",$username
        );
    }

    $list = '';
    foreach($shouts as $shout){
        $author = $shout['author'];
        $imagePath = $shout['imagePath'];
        $message = $shout['message'];

        $list .=
<<<ENDLIST
        <li>
            <div class="shoutDiv">
                <div class="shoutPhoto"
                    style="background-image: url('$imagePath')"></div>
                <p class="shoutAuthor">$author</p>
                <p class="shoutMsg">$message</p>
            </div>
        </li>
ENDLIST;
    }

    if(empty($shouts)){
        $list = "<li>There is no Shouts from $username</li>";
    }
    $response->getbody()->write($list);
    return $response;

});

$app->get('/register/isusernametaken/[{username}]', function (Request $request, Response $response, array $args) {
    $error = '';

    if (isset($args['username'])) {
        $error = isUsernameTaken($args['username']) ? "It's already taken." : '';
    }

    $response->getBody()->write($error);
    return $response;
});


$app->run();



function isValidPhoto($photo)
{
    if (!isset($photo['tmp_name']) || empty($photo['tmp_name'])) {
        return false;
    }

    $imageInfo = getimagesize($photo['tmp_name']);

    list($width, $height) = getimagesize($photo['tmp_name']);
    $mimeType = $imageInfo['mime'];

    if (
        $width < 50 || $width > 500
        || $height < 50 || $height > 500
    ) {
        return false;
    } else {
        switch ($mimeType) {
            case 'image/gif':
                return 'gif';
                break;
            case 'image/png':
                return 'png';
                break;
            case 'image/jpeg':
                return 'jpeg';
             case 'image/jpg':
                return 'jpeg';
                break;    break;
            default: // reject any other format
                return false;
        }
    }
}

function isUsernameTaken($username)
{
    $result = DB::queryFirstRow("SELECT COUNT(*) AS 'userCount' FROM users WHERE username = %s", $username);

    if ($result['userCount'] == 0) {
        return false;
    } elseif ($result['userCount'] == 1) {
        return true;
    } else {
        echo "Internal Error: duplicate username."; //FIXME : Log instead of echoing
        return true;
    }
}
