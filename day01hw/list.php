<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset="UTP-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
	<title>Add Person</title>
</head>
<body>

<?php 
	include "./db.php";
	
	$stmt = $conn->prepare('SELECT id,name,gpa,isGraduate,gender '
			. 'FROM day01hwpeople');

	$stmt->execute();
	$result = $stmt->fetchAll();
?>

<table border = 1>
<tr>
	<th>ID</th>
	<th>Name</th>
	<th>GPA</th>
	<th>Graduate</th>
	<th>Gender</th>
</tr>

<?php 
	foreach($result as $person)
	{
		$html = "<tr>\n"
			. "<td>" . $person['id'] . "</td>\n"
			. "<td>" . $person['name'] . "</td>\n"
			. "<td>" . $person['gpa'] . "</td>\n"
			. "<td>" . $person['isGraduate'] . "</td>\n"
			. "<td>" . $person['gender'] . "</td>\n"
			. "</tr>\n";
		echo $html;
	}
?>
</table>

</body>
</html>
