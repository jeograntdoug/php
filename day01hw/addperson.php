<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset="UTP-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
	<title>Add Person</title>

	<link rel="stylesheet" type="text/css" href="./style/addperson.css" />
</head>
<body>

<?php

	include "./db.php";

	$name = '';
	$GPA = '';
	$isGraduate = 'false';
	$gender = 'other';
	$errors = array();

	if(isset($_POST['name'])){
		$name = $_POST['name'];
		$GPA = $_POST['GPA'];
		$isGraduate = isset($_POST['isGraduate'])? 'true':'false';
		$gender = $_POST['gender'];

		if(strlen($name) < 2 || strlen($name) > 20) {
			$errors['name'] = "Name must be 2~20 chars.";
		}
		if(!is_numeric($GPA) || $GPA < 0 || $GPA > 4.3){
			$errors['GPA'] = "GPA must be 0~4.3.";
		}
		
		if(empty($errors))
		{
			// Insert data into database

			$stmt = $conn->prepare("INSERT INTO day01hwpeople (name,gpa,isGraduate,gender) Values (:name, :gpa, :isGraduate, :gender)");

			$stmt->bindParam(':name', $name);
			$stmt->bindParam(':gpa', $GPA);
			$stmt->bindParam(':isGraduate',$isGraduate );
			$stmt->bindParam(':gender', $gender);
			$stmt->execute();

			header('Location: list.php');
		}
	}
?>

<form method="POST">
	<ul>
	<?php foreach($errors as $error){ ?>
		<li><?php echo $error; ?></li>
	<?php }?>
	</ul>
	<div>
		<label>Name: </label>
		<input type="text" name="name" 
			value="<?php echo isset($errors['name'])?'':$name; ?>"/>
	</div>
	<div>
		<label>GPA: </label>
		<input 
			type="text" 
			name="GPA" 
			value="<?php echo isset($errors['GPA'])?'':$GPA; ?>" />
	</div>
	<div>
		<label>Is graduate</label>
		<input 
			type="checkbox" 
			name="isGraduate" 
			<?php echo $isGraduate == 'true'?'checked':''?> />
	</div>
	<div>
		<label>Gender: </label>

		<input 
			type="radio" 
			name="gender" 
			value="other" 
			checked
			/>
		<label>Other</label>

		<input 
			type="radio" 
			name="gender" 
			value="male" 
			<?php echo $gender == 'male'? 'checked':'' ?>
			/>
		<label>Male</label>

		<input 
			type="radio" 
			name="gender" 
			value="female" 
			<?php echo $gender == 'female'? 'checked':'' ?>
			/>
		<label>Female</label>
	</div>
	<div>
		<input type="submit" value="Add Person" />
	</div>
</form>

</body>
</html>

