<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

require __DIR__ . '/vendor/autoload.php';

DB::$user = 'day04slimfirst';
DB::$password = 'OGXt6mbq8VyZn3QN';
DB::$dbName = 'day04slimfirst';
DB::$port = 3333;

$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache' , 'debug' => true]);

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'hello.html.twig', [
        'name' => $args['name']
    ]);
});

$app->get('/sayhello', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'sayhello.html.twig');
});

$app->post('/sayhello', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $person = $request->getParsedBody();

    $name = $person['name'];
    $age = $person['age'];
    $errors = [];

    if(strlen($name) < 4 || strlen($name) > 100){
        $errors['name'] = "Name must be 4~100 chars.";
        $person['name'] = '';
    }

    if(!is_numeric($age)){
        $errors['age'] = "Age must be 1~100.";
        $person['age'] = '';
    } else if($age < 1 || $age > 100){
        $errors['age'] = "Age must be 1~100.";
        $person['age'] = '';
    }

    if($errors){
        return $view->render($response, 'sayhello.html.twig',[
            'person' => $person,
            'errors' => $errors
        ]);
    } else {
        DB::insert('people', [
            'name' => $person['name'],
            'age' => $person['age']
        ]);

        return $view->render($response, 'sayhello_success.html.twig', [
            'person' => $person
        ]);
    }
});

$app->run();