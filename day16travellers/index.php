<?php

require "setup.php";

use App\Validator;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;


$app->get('/api/travellers', function (Request $request, Response $response, array $args) {
    $travellerList = DB::query("SELECT * FROM travellers");
    $response->getBody()->write(json_encode($travellerList));
    return $response;
});

$app->get('/api/travellers/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $traveller = DB::query("SELECT * FROM travellers WHERE id=%s",$id);
    $response->getBody()->write(json_encode($traveller));
    return $response;
});

$app->post('/api/travellers', function (Request $request, Response $response, array $args) {
    $traveller = json_decode($request->getBody(),true);

    $errorList = Validator::traveller($traveller);
    $jsonMsg = '';

    // validation
    if(empty($errorList)){
        DB::insert('travellers',$traveller);
        $id = DB::insertId(); 
        $jsonMsg = json_encode($id);
    } else {
        $jsonMsg = json_encode($errorList);
    }

    $response->getBody()->write($jsonMsg);
    return $response;
});

$app->put('/api/travellers/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $traveller = json_decode($request->getBody(),true);
    
    // validation
    $errorList = Validator::traveller($traveller);
    $jsonMsg = '';

    if(empty($errorList)){
        DB::update('travellers',$traveller,'id=%s',$id);
        $jsonMsg = json_encode(DB::affectedRows());
    } else {
        $jsonMsg = json_encode($errorList);
    }

    $response->getBody()->write($jsonMsg);
    return $response;
});

$app->patch('/api/travellers/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $traveller = json_decode($request->getBody(),true);

    // validation
    $errorList = Validator::traveller($traveller,false);
    $jsonMsg = '';

    if(empty($errorList)){
        DB::update('travellers',$traveller,'id=%s',$id);
        $jsonMsg = json_encode(DB::affectedRows());
    } else {
        $jsonMsg = json_encode($errorList);
    }

    $response->getBody()->write($jsonMsg);
    return $response;
});

$app->delete('/api/travellers/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    DB::delete('travellers','id=%s',$id);
    $affectedRow = DB::affectedRows();

    $response->getBody()->write(json_encode($affectedRow));
    return $response;
});

$app->run();
