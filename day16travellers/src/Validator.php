<?php

namespace App;

use Respect\Validation\Validator as v;

class Validator
{
    public static function traveller($traveller, $isPut = true){
        $errorList = [];

        if(!v::notEmpty()->validate($traveller) && $isPut){
            return [
                'name' => '1-50 chars long.',
                'destination' => '1-50 chars long.',
                'departureDate' => 'Invalid Date.',
                'returnDate' => 'Invalid Date',
            ];
        }

        if(!v::key('name', v::stringVal()->length(1,50), $isPut)
                ->validate($traveller))
        {
            $errorList['name'] = '1-50 chars long.';
        }
        
        if(!v::key('destination', v::stringVal()->length(1,50), $isPut)
                ->validate($traveller))
        {
            $errorList['destination'] = '1-50 chars long.';
        }

        if(!v::key(
            'departureDate',
            v::date('Y-m-d')->between(date('1900-01-01'),date('2100-01-01'))
            ,$isPut
            )->validate($traveller))
        {
            $errorList['departureDate'] = 'Must be 1900-01-01 ~ 2100-01-01';
        } 

        if(!v::key(
            'returnDate',
            v::date('Y-m-d')->between(date('1900-01-01'),date('2100-01-01'))
            ,$isPut
            )->validate($traveller))
        {
            $errorList['returnDate'] = 'Must be 1900-01-01 ~ 2100-01-01';
        } 

        if(v::key('departureDate',v::notEmpty())->validate($traveller)
            && v::key('returnDate',v::notEmpty())->validate($traveller))
        {
            $dDate = new \DateTime($traveller['departureDate']);
            $rDate = new \DateTime($traveller['returnDate']);

            if(v::lessThan($dDate)->validate($rDate)){
                $errorList['returnDate'] = 'Must be after departureDate';
            }
        }

        return $errorList;
    }
}