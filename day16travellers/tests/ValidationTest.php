<?php

use PHPUnit\Framework\TestCase;
use App\Validator;

class ValidationTest extends TestCase
{
    protected $v;

    public static function setUpBeforeClass(): void
    {
    }

    public static function tearDownAfterClass(): void
    {
    }

    protected function setUp():void
    {
        $this->v = new Validator();
    }

    protected function tearDown():void
    {
    }

    /** @test */
    public function putRequest_EmptyTravellerIsNotValid(){
        $traveller = [];
        $isPut = true;
        $this->assertNotEmpty($this->v->traveller($traveller,$isPut));
        $this->assertCount(4,$this->v->traveller($traveller,$isPut));

        $traveller = [
            'name' => '',
            'destination' => '',
            'departureDate' => '',
            'returnDate' => '',
        ];
        $this->assertNotEmpty($this->v->traveller($traveller,$isPut));
        $this->assertCount(4,$this->v->traveller($traveller,$isPut));

        $traveller = [
            'name' => null,
            'destination' => null,
            'departureDate' => null,
            'returnDate' => null,
        ];
        $this->assertNotEmpty($this->v->traveller($traveller,$isPut));
        $this->assertCount(4,$this->v->traveller($traveller,$isPut));
    }



    /** @test */
    public function putRequest_travellerWithEmptyFieldIsNotValid(){
        $isPut = true;
        $traveller = [
            'name' => '',
            'destination' => '',
            'departureDate' => '2010-01-01',
            'returnDate' => '2011-01-01',
        ];
        $errorList = $this->v->traveller($traveller,$isPut);
        $this->assertCount(2,$errorList);
        $this->assertArrayHasKey('name',$errorList);
        $this->assertArrayHasKey('destination',$errorList);

        $traveller = [
            'name' => 'Jerry',
            'destination' => 'Montreal',
            'departureDate' => '',
            'returnDate' => '',
        ];
        $errorList = $this->v->traveller($traveller,$isPut);
        $this->assertCount(2,$errorList);
        $this->assertArrayHasKey('departureDate',$errorList);
        $this->assertArrayHasKey('returnDate',$errorList);

        $traveller = [
            'name' => null,
            'destination' => null,
            'departureDate' => '2010-01-01',
            'returnDate' => '2011-01-01',
        ];
        $errorList = $this->v->traveller($traveller,$isPut);
        $this->assertCount(2,$errorList);
        $this->assertArrayHasKey('name',$errorList);
        $this->assertArrayHasKey('destination',$errorList);

        $traveller = [
            'name' => 'Jerry',
            'destination' => 'Montreal',
            'departureDate' => null,
            'returnDate' => null,
        ];
        $errorList = $this->v->traveller($traveller,$isPut);
        $this->assertCount(2,$errorList);
        $this->assertArrayHasKey('departureDate',$errorList);
        $this->assertArrayHasKey('returnDate',$errorList);
    }

    /** @test */
    public function patchRequest_individualValidation(){
        $isPut = false;
        $traveller = [];

        $errorList = $this->v->traveller($traveller,$isPut);
        $this->assertEmpty($errorList);

        $travellerList = [
            ['name' => ''],
            ['name' => 'lllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll'],
        ];
        foreach($travellerList as $traveller)
        {
            $errorList = $this->v->traveller($traveller,$isPut);
            $this->assertCount(1,$errorList);
            $this->assertArrayHasKey('name',$errorList);
        }

        $travellerList = [
            ['destination' => ''],
            ['destination' => 'lllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll'],
        ];
        foreach($travellerList as $traveller)
        {
            $errorList = $this->v->traveller($traveller,$isPut);
            $this->assertCount(1,$errorList);
            $this->assertArrayHasKey('destination',$errorList);
        }

        $travellerList = [
            ['departureDate' => '1899-12-31'],
            ['departureDate' => '2100-01-02'],
            ['departureDate' => ''],
            ['departureDate' => null],
        ];
        foreach($travellerList as $traveller)
        {
            $errorList = $this->v->traveller($traveller,$isPut);
            $this->assertCount(1,$errorList);
            $this->assertArrayHasKey('departureDate',$errorList);
        }

        $travellerList = [
            ['returnDate' => '1899-12-31'],
            ['returnDate' => '2100-01-02'],
            ['returnDate' => ''],
            ['returnDate' => null],
        ];
        foreach($travellerList as $traveller)
        {
            $errorList = $this->v->traveller($traveller,$isPut);
            $this->assertCount(1,$errorList);
            $this->assertArrayHasKey('returnDate',$errorList);
        }
    }

    /** @test */
    public function departureDateAndReturnDate(){
        $isPut = false;
        $travellerList = [
            [
                'departure' => '1900-12-31',
                'returnDate' => '1900-12-31'
            ],
            [
                'departure' => '1900-01-01',
                'returnDate' => '2100-01-01'
            ],
        ];
        foreach($travellerList as $traveller)
        {
            $errorList = $this->v->traveller($traveller,$isPut);
            $this->assertEmpty($errorList);
        }

        $traveller = [
            'departureDate' => '2100-01-01',
            'returnDate' => '1900-01-01'
        ];

        $errorList = $this->v->traveller($traveller,$isPut);
        $this->assertCount(1,$errorList);
        $this->assertArrayHasKey('returnDate',$errorList);
    }
}