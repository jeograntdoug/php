<?php

namespace App\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class HomeController
{
    public function home(Request $request, Response $response, $args = [])
    {
        $response->getBody()->write('Welcome, Home');
        return $response;
    }

    public function hello(Request $request, Response $response, $args = [])
    {
        $response->getBody()->write('Hello!');
        return $response;
    }

    public function jsondata(Request $request, Response $response, $args = [])
    {
        $data = array('name' => 'Rob', 'age' => 40);

        $response->getBody()->write("alskdflkasjdflkjasdfkljlk");
        return $response;
    }
}
