<?php

use App\Controller\HomeController;
use App\Controller\ErrorController;
use Psr\Http\Message\ResponseFactoryInterface;
use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Routing\RouteCollectorProxy;
use Slim\Views\Twig;

return function(App $app){
    // Routes

    $beforeMiddleware = function ($request, $handler) {
        $response = $handler->handle($request);
        $existingContent = (string) $response->getBody();

        //$response = new Response();
        $response->getBody()->write('BEFORE' . $existingContent);

        return $response;
    };

    $app->add($beforeMiddleware);

    $app->get('/', HomeController::class .  ':home');
    $app->get('/hello', HomeController::class . ':hello');
    $app->get('/jsondata', HomeController::class . ':jsondata');

    //Routes for Errors Pages
    $app->group('/errors', function (RouteCollectorProxy $group) {
    // ??: Couldn't use /error url. Is it reserved??
        $group->get('/forbidden', ErrorController::class . ':forbidden');
        $group->get('/internal', ErrorController::class . ':internal');
    });

};