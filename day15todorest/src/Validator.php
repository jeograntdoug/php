<?php
namespace App;

use PDO;
use Respect\Validation\Validator as v;

class Validator
{
    private $userVarlidator;

    public function __construct(){
    }
    public function validateTodo($todo, $forPatch = false) {

        global $log;
        if (v::nullType()->validate($todo)) { // if json_decode fails it returns null - handle it here
            return "Invalid JSON data provided";
        }
        // does it have all the fields required and only the fields requried?
        $expectedFields = ['task', 'dueDate', 'isDone'];

        $newTodo = [];
        foreach($todo as $key => $value){
            if($value !== null){
                $newTodo[$key] = $value;
            }
        }
        $todo = $newTodo;

        if (!v::notEmpty()->validate($todo)) { // if json_decode fails it empty array or empty values with key
            return "Invalid JSON data provided";
        }


        $todoFields = array_keys($todo);
        // check for fields that should not be there
        if (($diff = array_diff($todoFields, $expectedFields))) {
            return "Invalid fields in Todo: [" . implode(',',$diff) . "]";
        }
        if (!$forPatch) {
        // check for fields that are missing
            if (($diff = array_diff($expectedFields, $todoFields))) {
                return "Missing fields in Todo: [" . implode(',',$diff) . "]";
            }
        }

        if(!v::key('task',v::stringVal()->length(1,100), false)->validate($todo)){
                return "Task description must be 1-100 characters long";
        }

        // // FIXME: do not allow any field to be null
        // if (v::key('task')->validate($todo)) { // in patch it may be absent
        //     $task = $todo['task'];
        //     if (!v::stringVal()->length(1,100)->validate($task)) {
        //         return "Task description must be 1-100 characters long";
        //     }
        // }


        if(!v::key('dueDate',v::datetime('Y-m-d')->between('1900-01-01','2100-01-01'), false)->validate($todo)){
                return "DueDate must be within 1900 to 2099 years";
        }

        // if (v::key('dueDate')->validate($todo)) {
        //     if(//!v::notEmpty()->validate($todo['dueDate']) ||
        //          !v::datetime('Y-m-d')->between('1900-01-01','2100-01-01')->validate($todo['dueDate'])){
        //         return "DueDate must be within 1900 to 2099 years";
        //     }
        // }

        if(!v::key('isDone',v::containsAny(['pending','done']), false)->validate($todo)){
                return "IsDone invalid: must be pending or done";
        }

        // if (v::key('isDone')->validate($todo)) {
        //     if(!v::containsAny(['pending','done'])->validate($todo['isDone'])){
        //         return "IsDone invalid: must be pending or done";
        //     }
        // }
        //
        return TRUE;
    }


    public function validateTodoOld($todo, $forPatch = false) {
        global $log;
        if ($todo === NULL) { // if json_decode fails it returns null - handle it here
            return "Invalid JSON data provided";
        }
        // does it have all the fields required and only the fields requried?
        $expectedFields = ['task', 'dueDate', 'isDone'];

        $newTodo = [];
        foreach($todo as $key => $value){
            if($value !== null){
                $newTodo[$key] = $value;
            }
        }
        $todo = $newTodo;

        $todoFields = array_keys($todo);
        // check for fields that should not be there
        if (($diff = array_diff($todoFields, $expectedFields))) {
            return "Invalid fields in Todo: [" . implode(',',$diff) . "]";
        }
        if (!$forPatch) {
        // check for fields that are missing
            if (($diff = array_diff($expectedFields, $todoFields))) {
                return "Missing fields in Todo: [" . implode(',',$diff) . "]";
            }
        }

        // FIXME: do not allow any field to be null
        // validate each field
        if (isset($todo['task'])) { // in patch it may be absent
            $task = $todo['task'];
            if (strlen($task) < 1 || strlen($task) > 100) {
                return "Task description must be 1-100 characters long";
            }
        }
        if (isset($todo['dueDate'])) {
            if (!date_create_from_format('Y-m-d', $todo['dueDate'])) {
                return "DueDate has invalid format";
            }
            $dueDate = strtotime($todo['dueDate']);
            if ($dueDate < strtotime('1900-01-01') || $dueDate > strtotime('2100-01-01')) {
                return "DueDate must be within 1900 to 2099 years";
            }
        }
        if (isset($todo['isDone'])) {
            if (!in_array($todo['isDone'], ['pending', 'done'])) {
                return "IsDone invalid: must be pending or done";
            }
        }
        //
        return TRUE;
    }

    public function validateAll($todo,$isPatch = false){
        if (v::nullType()->validate($todo)) { // if json_decode fails it returns null - handle it here
            return false;
        }

        $newTodo = [];
        foreach($todo as $key => $value){
            if($value !== null){
                $newTodo[$key] = $value;
            }
        }
        $todo = $newTodo;

        if(!v::notEmpty()->validate($todo)){
            return false;
        }

        return v::keySet(
            v::key('task', v::stringVal()->length(1,100), !$isPatch),
            v::key('dueDate', v::datetime('Y-m-d')->between('1900-01-01','2100-01-01'), !$isPatch),
            v::key('isDone',v::containsAny(['pending','done']), !$isPatch)
        )->validate($todo);
    }
}