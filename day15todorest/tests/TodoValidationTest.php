<?php

use PHPUnit\Framework\TestCase;
use App\Validator;

require('vendor/autoload.php');

class TodoValidationTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        DB::$user = 'day15todorest';
        DB::$password = 'T8iUxF07dPgNUG2k';
        DB::$dbName = 'day15todorest';
        DB::$port = 3333;
        DB::$encoding = 'utf8'; // defaults to latin1 if omitted
    }

    public static function tearDownAfterClass(): void
    {
        DB::query('DELETE FROM todos');
    }

    protected function setUp():void
    {
    }

    protected function tearDown():void
    {
    }

    /**  @test */
    public function testValidateTodoInPutMethod(){
        $v = new Validator();

        $this->assertEquals("Invalid JSON data provided", $v->validateTodo(null,true));
        $this->assertFalse($v->validateAll(null));

        $emptyTodoList = [
            [],
            [
                'task' => null,
                'dueDate' => null,
                'isDone' => null 
            ],
        ];

        foreach($emptyTodoList as $todo)
        {
            $this->assertEquals("Invalid JSON data provided", $v->validateTodo($todo,true));
            $this->assertFalse($v->validateAll($todo));
        }

        $todo = [
            'task' =>'Play Piano',
            'dueDate' =>  '2020-11-11',
            'isDone' => 'pending',
        ];

        $this->assertTrue($v->validateTodo($todo));
        $this->assertTrue($v->validateAll($todo));

        $invalidTodoListForTask = [
            //test task
            [// task length = 101
                'task' =>'lllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll',
                'dueDate' =>  '2020-11-11',
                'isDone' => 'pending'
            ],
            [// task empty
                'task' =>'',
                'dueDate' =>  '2020-11-11',
                'isDone' => 'pending'
            ],
        ];

        foreach($invalidTodoListForTask as $todo)
        {
            $result = $v->validateTodo($todo);
            $this->assertTrue( preg_match('/Task/',$result) === 1 );
            $this->assertFalse($v->validateAll($todo));
        }

        $invalidTodoListForDueDate = [
            //test dueDate
            [
                'task' =>'Play Piano',
                'dueDate' =>  '',
                'isDone' => 'pending'
            ],
            [
                'task' =>'Play Piano',
                'dueDate' =>  '2000/01/02',
                'isDone' => 'pending'
            ],
            [
                'task' =>'Play Piano',
                'dueDate' =>  '2100-01-02',
                'isDone' => 'pending'
            ],
            [
                'task' =>'Play Piano',
                'dueDate' =>  '1899-12-31',
                'isDone' => 'pending'
            ],
        ];

        foreach($invalidTodoListForDueDate as $todo)
        {
            $result = $v->validateTodo($todo);
            $this->assertTrue( preg_match('/DueDate/',$result) === 1 );
            $this->assertFalse($v->validateAll($todo));
        }

        $invalidTodoListForIsDone = [
            [
                'task' =>'Play Piano',
                'dueDate' =>  '2020-11-11',
                'isDone' => 'laskdflkdj'
            ],
            [
                'task' =>'Play Piano',
                'dueDate' =>  '2020-11-11',
                'isDone' => ''
            ],
        ];

        foreach($invalidTodoListForTask as $todo)
        {
            $result = $v->validateTodo($todo);
            $this->assertTrue( preg_match('/Task/',$result) === 1 );
            $this->assertFalse($v->validateAll($todo));
        }

        $missingTaskTodoList = [
            [
                'task' => null,
                'dueDate' =>  '2020-11-11',
                'isDone' => 'pending'
            ],
            [
                'dueDate' =>  '2020-11-11',
                'isDone' => 'pending'
            ],
        ];

        foreach($missingTaskTodoList as $todo)
        {
            $result = $v->validateTodo($todo);
            $this->assertTrue( 
                preg_match( '/^Missing fields in Todo: \[task\]$/',$result) === 1 
            );
            $this->assertFalse($v->validateAll($todo));
        }

        $missingDueDateTodoList = [
            [
                'task' =>'Play Piano',
                'isDone' => 'pending'
            ],
            [
                'task' =>'Piano',
                'dueDate' =>  null,
                'isDone' => 'pending'
            ],
        ];

        foreach($missingDueDateTodoList as $todo)
        {
            $result = $v->validateTodo($todo);
            $this->assertTrue( 
                preg_match( '/^Missing fields in Todo: \[dueDate\]$/',$result) === 1 
            );
            $this->assertFalse($v->validateAll($todo));
        }

        $missingTaskIsDoneList = [
            [
                'task' =>'Piano',
                'dueDate' =>  '2020-11-11',
                'isDone' => null
            ],
            [
                'task' =>'Play Piano',
                'dueDate' =>  '2020-11-11',
            ],
        ];

        foreach($missingTaskIsDoneList as $todo)
        {
            $result = $v->validateTodo($todo);
            $this->assertTrue( 
                preg_match( '/^Missing fields in Todo: \[isDone\]$/',$result) === 1 
            );
            $this->assertFalse($v->validateAll($todo));
        }

        $todo = [
            'task' =>'Play Piano',
            'dueDate' =>  '2020-11-11',
            'isDone' => 'pending',
            'extraField' => 'extraValue'
        ];

        $result = $v->validateTodo($todo);
        $this->assertTrue( 
            preg_match( '/^Invalid fields in Todo: \[extraField\]$/',$result) === 1 
        );
        $this->assertFalse($v->validateAll($todo));
    }


    /**  @test */
    public function testValidateTodoInPatchMethod(){
        $v = new Validator();

        $this->assertEquals("Invalid JSON data provided", $v->validateTodo(null,true));
        $this->assertFalse($v->validateAll(null,true));

        $invalidTodoListForTask = [
            //test task
            [
                'task' =>'hahahaha',
            ],
            [
                'task' =>'hohohoho',
                'dueDate' =>  '2020-11-11',
            ],
            [
                'task' =>'hohohoho',
                'isDone' => 'pending'
            ],
            [
                'task' => null,
                'dueDate' =>  '2020-11-11',
            ],
        ];

        foreach($invalidTodoListForTask as $todo)
        {
            $this->assertTrue($v->validateTodo($todo,true));
            $this->assertTrue($v->validateAll($todo,true));
        }

        $invalidTodoListForDueDate = [
            //test dueDate
            [
                'dueDate' =>  '2020-11-11',
            ],
            [
                'dueDate' =>  '2020-11-11',
                'isDone' => 'pending'
            ],
            [
                'task' =>'Play Piano',
                'dueDate' =>  '2020-11-11',
            ],
            [
                'dueDate' => null,
                'isDone' => 'pending'
            ],
        ];

        foreach($invalidTodoListForDueDate as $todo)
        {
            $this->assertTrue( $v->validateTodo($todo,true));
            $this->assertTrue($v->validateAll($todo,true));
        }

        $invalidTodoListForIsDone = [
            [
                'isDone' => 'pending'
            ],
            [
                'dueDate' =>  '2020-11-11',
                'isDone' => 'pending'
            ],
            [
                'task' =>'Play Piano',
                'isDone' => 'pending'
            ],
            [
                'dueDate' =>  '2020-11-11',
                'isDone' => null 
            ],
        ];

        foreach($invalidTodoListForTask as $todo)
        {
            $this->assertTrue( $v->validateTodo($todo,true));
            $this->assertTrue($v->validateAll($todo,true));
        }

        $emptyTodoList = [
            [
            ],
            [
                'task' => null,
                'dueDate' => null,
                'isDone' => null 
            ],
        ];

        foreach($emptyTodoList as $todo)
        {
            $this->assertEquals("Invalid JSON data provided", $v->validateTodo($todo,true));
            $this->assertFalse($v->validateAll($todo,true));
        }
    }
}