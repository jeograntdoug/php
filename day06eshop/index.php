<?php

require "setup.php";

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Routing\RouteCollectorProxy;
use Slim\Views\Twig;

const PRODUCTS_PER_PAGE = 5;

$app->get('/forbidden', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_internal', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/', function (Request $request, Response $response, array $args) {

    $categories = DB::query( "SELECT * FROM categories");
    $view = Twig::fromRequest($request);

    $currentPage = 1;
    $productStart = 0;

    $totalProducts = DB::queryFirstRow("SELECT COUNT(*) AS 'count' FROM products")['count'];
    $totalPages = ceil($totalProducts / PRODUCTS_PER_PAGE);
    
    $get = $request->getQueryParams();
    if(isset($get['page'])){
        $currentPage = $get['page'];
    }

    //TODO : Create new error page
    if($currentPage < 1){
        $currentPage = 1;
    } else if($currentPage > $totalPages){
        $currentPage = $totalPages;
    }

    $productStart = ($currentPage - 1) * PRODUCTS_PER_PAGE;

    $products = DB::query(
        "SELECT p.id AS 'productId', p.name AS 'productName'
            , p.description, p.unitPrice, p.pictureFilePath
            , c.name AS 'categoryName'
        FROM products AS p
        JOIN categories AS c 
        ON c.id = p.categoryId
        ORDER BY p.id
        LIMIT %i,%i ",$productStart, PRODUCTS_PER_PAGE
    );

    return $view->render($response, 'index.html.twig',[
        'products' => $products,
        'currentPage' => $currentPage,
        'totalPages' => $totalPages,
        'categories' => $categories
    ]);
});

$app->get('/category/{id:[0-9]+}', function (Request $request, Response $response, array $args) {

    $view = Twig::fromRequest($request);

    $categories = DB::query( "SELECT * FROM categories");

    $categoryId = $args['id'];

    $totalProducts = DB::queryFirstRow(
        "SELECT COUNT(*) AS 'count' 
        FROM products
        WHERE categoryId = %i",$categoryId
    )['count'];

    $totalPages = ceil($totalProducts / PRODUCTS_PER_PAGE);
    
    $get = $request->getQueryParams();

    $currentPage = 1;
    $productStart = 0;

    if(isset($get['page'])){
        $currentPage = $get['page'];
    }

    if($currentPage < 1){
        $currentPage = 1;
    } else if($currentPage > $totalPages){
        $currentPage = $totalPages;
    }

    $productStart = ($currentPage - 1) * PRODUCTS_PER_PAGE;

    $products = DB::query(
        "SELECT p.id AS 'productId', p.name AS 'productName'
            , p.description, p.unitPrice, p.pictureFilePath
            , c.name AS 'categoryName'
        FROM products AS p
        JOIN categories AS c 
        ON c.id = p.categoryId
        WHERE c.id = %i
        LIMIT %i,%i ",$categoryId, $productStart, PRODUCTS_PER_PAGE
    );

    return $view->render($response, 'index.html.twig',[
        'products' => $products,
        'currentPage' => $currentPage,
        'totalPages' => $totalPages,
        'categories' => $categories
    ]);
});

$app->get('/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        //TODO: Add login message
        return $response->withHeader('Location', '/');
    }

    return $view->render($response, 'login.html.twig');
});

$app->post('/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $loginInfo = $request->getParsedBody();

    if ($loginInfo != null) {
        if (isset($loginInfo['email']) && isset($loginInfo['password'])) {
            $user = DB::queryFirstRow("SELECT * FROM users WHERE email= %s", $loginInfo['email']);
            if ($loginInfo['password'] === $user['password']) {
                unset($user['password']);
                $_SESSION['user'] = $user;

                return $response
                    ->withHeader('Location', '/');
            }
        }
    }

    return $view->render($response, 'login.html.twig', [
        'error' => "Email doesn't match password."
    ]);
});

$app->get('/logout', function (Request $request, Response $response) {
    unset($_SESSION['user']);
    return $response->withHeader('Location', '/');
});

$app->get('/register', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});

$app->post('/register', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $registerInfo = $request->getParsedBody();

    $email = $registerInfo['email'];
    $name = $registerInfo['name'];
    $password = $registerInfo['password'];
    $errors = [];

    if (strlen($name) < 5 || strlen($name) > 20) {
        $errors['name'] = "Name must be 5~20 chars";
        $registerInfo['name'] = '';
    } 

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $errors['email'] = "Invalid Email";
        $email = '';
    } elseif (isEmailTaken($email)) {
        $errors['email'] = "User is already exist.";
        $email= '';
    }


    if (strlen($password) < 6 || strlen($password) > 100
        || preg_match("/[a-z]/", $password) == false
        || preg_match("/[A-Z]/", $password) == false
        || preg_match("/[0-9#$%^&*()+=-\[\]';,.\/{}|:<>?~]/", $password) == false) {
        $errors['password'] = "Password must be 6~100 characters,
                            must contain at least one uppercase letter, 
                            one lower case letter, 
                            and one number or special character.";
    } elseif ($password !== $_POST['confirm']) {
        $errors['password'] = "Passwords must be same.";
    }

    if (empty($errors)) {
        DB::insert('users', [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'isAdmin' => 'false'
        ]);
        $_SESSION['user'] = DB::queryFirstRow("SELECT * FROM users WHERE email = %s",$email);

        return $response->withHeader('Location', '/');
    }

    return $view->render($response, 'register.html.twig', [
        'errors' => $errors,
        'prevInput' => [
            'name' => $name,
            'email' => $email
        ]
    ]);
});

$app->get('/register/isemailtaken/[{email}]', function (Request $request, Response $response, array $args){
    $error = '';

    if(isset($args['email'])){
        $error = isEmailTaken($args['email']) ? "It's already taken." :'';
    }

    $response->getBody()->write($error);
    return $response;
});


$app->get('/addToCart', function (Request $request, Response $response) {

    $sessionId = $_COOKIE['PHPSESSID'];
    $get = $request->getQueryParams();

    $message = 'failed';
    if(isset($get['quantity']) && isset($get['productId'])){

        $productId = $get['productId'];
        
        if($get['quantity'] > 0){
            $product = DB::queryFirstRow(
                "SELECT id
                FROM products 
                WHERE id = %i",$productId
            );


            if( isset($product['id'])){

                $quantity = $get['quantity'];

                $cartItem = DB::queryFirstRow(
                    "SELECT quantity 
                    FROM cartItems 
                    WHERE sessionId = %s
                    AND productId = %i", $sessionId, $productId
                );

                if(isset($cartItem['quantity'])){
                    $quantity += $cartItem['quantity'];
                }

                DB::insertUpdate("cartItems",[
                    'sessionId' => $sessionId,
                    'productId' => $productId,
                    'quantity' => $quantity
                ]);

                $message = "succeed";
            }
        }
    }
    
    $response->getBody()->write($message);
    return $response;
});

$app->get('/cart', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'cart.html.twig');
});

$app->get('/ajax/cartitems/list/{page:[0-9]+}', function (Request $request, Response $response, array $args) {

    $page = $args['page'];
    $sessionId = $_COOKIE['PHPSESSID'];
    $totalItems = DB::queryFirstField(
        "SELECT COUNT(*) 
        FROM cartItems
        WHERE sessionId = %s",$sessionId
    );

    $totalPages = ceil($totalItems / PRODUCTS_PER_PAGE);

    if($page < 1 || $page > $totalPages){
        return $response->withStatus(403);
    }

    $view = Twig::fromRequest($request);

    $pageSkip = ( $page - 1 ) * PRODUCTS_PER_PAGE;

    $itemList = DB::query(
        "SELECT p.name AS 'name', p.description,
        p.unitPrice, p.pictureFilePath, p.id AS 'id', i.quantity
        FROM cartItems AS i
        JOIN products AS p
        ON p.id = i.productId
        WHERE i.sessionId = %s
        LIMIT %i,%i", $sessionId, $pageSkip, PRODUCTS_PER_PAGE
    );
        


    return $view->render($response, 'cartitem.html.twig',[
        'productList' => $itemList,
        'totalPages' => $totalPages,
        'currentPage' => $page
    ]);
});

$app->get('/ajax/cartitems/delete/{id:[0-9]+}', function (Request $request, Response $response, array $args) use ($log) {
    if(isset($_COOKIE['PHPSESSID'])){

        DB::query(
            "DELETE FROM cartItems
            WHERE sessionId = %s
            AND productId = %i",
            $_COOKIE['PHPSESSID'],
            $args['id']
        );
    }

    $result = DB::affectedRows();

    if($result == 0){
        $response->getBody()->write('false');
        $log->error("Datebase[CartItemDelete]: Product Id[" . $args['id'] . "] and SessionId doesn't match(might be invalid attempt from client)");
    } else{
        $response->getBody()->write('true');
    }

    return $response;
});

$app->get('/admin/products/list', function(Request $request, Response $response) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] == 'false'){
        return $response->withHeader('Location','/forbidden');
    }

    $view = Twig::fromRequest($request);

    $totalProducts = DB::queryFirstField("SELECT COUNT(*) FROM products");
    $totalPages = ceil($totalProducts / PRODUCTS_PER_PAGE);

    return $view->render($response, '/admin/product_list.html.twig',[
        'totalPages' => $totalPages
    ]);
});

$app->get('/admin/ajax/products/list/{page:[0-9]+}',function(Request $request, Response $response, array $args) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] == 'false'){
        return $response->withHeader('Location','/forbidden',403);
    }
    $view = Twig::fromRequest($request);

    $page = $args['page'];
    $pageSkip = ( $page - 1 ) * PRODUCTS_PER_PAGE;
    $productList = DB::query( "SELECT * FROM products LIMIT %i,%i", $pageSkip, PRODUCTS_PER_PAGE);
        
    return $view->render($response, '/admin/product.html.twig',[
        'productList' => $productList
    ]);
});

$app->get('/admin/ajax/products/delete/{id:[0-9]+}',function(Request $request, Response $response, array $args) use ($log) {

    if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] == 'false'){
        return $response->withHeader('Location','/forbidden');
    }

    $pictureFilePath = DB::queryFirstField(
        "SELECT pictureFilePath 
        FROM products
        WHERE id=%i",$args['id']
    );

    if(isset($pictureFilePath)){
        DB::delete('products', 'id=%i', $args['id']);

        deletePicture($pictureFilePath);

        $response->getBody()->write('true');

    } else{
        $response->getBody()->write('false');
        $log->error("Datebase[Delete]: Product Id[" . $args['id'] . "]doesn't match(might be invalid attempt from client)");
    }

    return $response;
});


$app->get('/admin/products/{action:add|edit}', function(Request $request, Response $response, array $args) use($log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] == 'false'){
        return $response->withHeader('Location','/forbidden');
    }

    $view = Twig::fromRequest($request);
    $action = $args['action'];
    $get = $request->getQueryParams();
    $product = '';

    if($action == 'edit'){

        $product = DB::queryFirstRow("SELECT * FROM products WHERE id=%i",$get['id'] ?? '');

        if(!isset($get['id'])){
            $log->error("Invlid access to '/admin/products/edit' without product id");
        }else if(empty($product)){
            return $response->withHeader('Location', '/forbidden', 403);
        }
    }

    return $view->render($response, '/admin/product_addedit.html.twig',[
        'action' => $action,
        'product' => $product
    ]);
});

$app->post('/admin/products/{action:add|edit}', function(Request $request, Response $response, array $args) use($log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] == 'false'){
        return $response->withHeader('Location','/forbidden');
    }
    $view = Twig::fromRequest($request);

    $action = $args['action'];
    $newProduct = $request->getParsedBody();
    $id = isset($newProduct['id'])? $newProduct['id'] : '';
    $name = $newProduct['name'];
    $description = $newProduct['description'];
    $category = $newProduct['categoryId'];
    $image = empty($_FILES['productFilePath']['error'])? $_FILES['productFilePath'] : '';
    $imgExtArray = ['image/jpeg','image/png','image/gif','image/jpg'];
    $categoryArray = ['simple','letters','steel','extra'];
    $errors = [];

    if(!preg_match('/^[a-zA-Z0-9,_ -]{5,20}$/', $name)){
        $errors['name'] = "Name must be 'a-zA-Z0-9_-' characters 5~20.";
        $name = '';
    }

    if(strlen($description) < 5 || strlen($description) > 1000){
        $errors['description'] = "Description must be 5~1000 characters.";
    }

    if($category > 4 || $category < 1){
        $errors['category'] = "Choose one of the category from the list.";
        $category = 1;
    }

    if(empty($errors)){
        $categoryName = $categoryArray[$category - 1];
        switch($action){
            case 'add':
                if(!empty($image)){ 
                    $newProduct['pictureFilePath'] = "/products_image/$categoryName/" . $image['name'];

                    DB::insert('products', $newProduct);
                    move_uploaded_file($image['tmp_name'], __DIR__ . $newProduct['pictureFilePath'] );
                }else {
                    $errors['image'] = "Chose Product Picture.";
                }
                break;
            case 'edit':
                $oldProduct = DB::queryFirstRow("SELECT * FROM products WHERE id=%i", $id);

                if(!empty($image)){ 
                    $newProduct['pictureFilePath'] = "/products_image/$categoryName/" . $image['name'];

                    deletePicture($oldProduct['pictureFilePath']);

                    move_uploaded_file($image['tmp_name'], __DIR__ . $newProduct['pictureFilePath'] );
                }else {
                    $newProduct['pictureFilePath'] = "/products_image/$categoryName/" . basename($oldProduct['pictureFilePath']);

                    rename( __DIR__ . $oldProduct['pictureFilePath'], __DIR__ . $newProduct['pictureFilePath'] );
                }
                
                DB::update('products', $newProduct,"id=%i",$id);
            break;
            default:
                $log->error("Internal Error: Action must be only add or edit");
            break;
        }
    } 


    $totalProducts = DB::queryFirstField("SELECT COUNT(*) FROM products");
    $totalPages = ceil($totalProducts / PRODUCTS_PER_PAGE);

    return $view->render($response, '/admin/product_list.html.twig',[
        'totalPages' => $totalPages
    ]);
});

$app->run();


function isEmailTaken($email)
{
    $users = DB::queryFirstRow("SELECT COUNT(*) AS 'count' FROM users WHERE email = %s", $email);

    if ($users['count'] == 0) {
        return false;
    } elseif ($users['count'] == 1) {
        return true;
    } else {
        echo "Internal Error: duplicate username.";//FIXME : Log instead of echoing
        return true;
    }
}

function deletePicture($filePath){
    if(empty($filePath)){
        return;
    }

    // NOTE: move file to deleted folder to keep file
    rename(__DIR__ . $filePath ,__DIR__ . '/products_image/deleted/'. basename($filePath));
}