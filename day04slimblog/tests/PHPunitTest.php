<?php declare(strict_types = 1);


use ipd20\day04slimblog\App;
use PHPUnit\Framework\TestCase;
use Slim\Factory\Psr17\SlimHttpServerRequestCreator;
use Slim\Psr7\Environment;
use Slim\Psr7\Factory\RequestFactory;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\ServerRequestFactory;

class PHPUnitTest extends TestCase
{
    // public function testGetTest(){
    //     $req = RequestFactory::createRequest('GET','/');
    //     $this->app->getContainer()['request'] = $req;
    //     $response = $this->app->run(true);
    //     $this->assertSame($response->getStatusCode(), 200);
    //     $this->assertContains($response->getBody(),"login" );
    // }

    public function test(){
        $echoController = new \App\Controller\EchoAction();
        $requestFactory = new ServerRequestFactory();
        $responseFactory = new ResponseFactory();

        $request = $requestFactory->createServerRequest('POST','/echo');
        $response = $responseFactory->createResponse();

        $response = $echoController->watch($request,$response);
        $this->assertEquals((string)$response->getBody(), 'msg');
    }
}