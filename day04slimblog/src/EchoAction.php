<?php
namespace App;

use Slim\App;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
 
final class EchoAction
{

    public function watch(Request $request, Response $response, $args = [])
    {
        $response->getBody()->write('msg');
        return $response;
    }
}