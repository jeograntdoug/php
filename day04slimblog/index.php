<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use App\EchoAction;

use Slim\Exception\HttpNotFoundException;

require __DIR__ . '/vendor/autoload.php';

session_start();

$app = AppFactory::create();

// Define Custom Error Handler
$customErrorHandler = function (
    Psr\Http\Message\ServerRequestInterface $request,
    \Throwable $exception,
    bool $displayErrorDetails,
    bool $logErrors,
    bool $logErrorDetails
) use ($app) {
    $response = $app->getResponseFactory()->createResponse();
    // seems the followin can be replaced by your custom response
    // $page = new Alvaro\Pages\Error($c);
    // return $page->notFound404($request, $response);

    return $response->withHeader('Location','/forbidden',404);
};

// Add Error Middleware
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
// Register the handler to handle only  HttpNotFoundException
// Changing the first parameter registers the error handler for other types of exceptions
$errorMiddleware->setErrorHandler(Slim\Exception\HttpNotFoundException::class, $customErrorHandler);


// create a log channel

$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));
$log->pushHandler(new StreamHandler('logs/invalidattempt.log', Logger::NOTICE));


DB::$user = 'day04slimblog';
DB::$password = 'Mq0X3YYFZGaxVpPx';
DB::$dbName = 'day04slimblog';
DB::$port = 3333;
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)


function db_error_handler($params)
{
    header("Location: /error_internal", 500);

    global $log;
    $log->error("Database erorr[Connection]: " . $params['error']);

    if ($params['query']) {
        $log->error("Database error[Query]: " . $params['query']);
    }
}

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache', 'debug' =>true]);

// Set Global variable($_SESSION)
$twig->getEnvironment()->addGlobal('session', $_SESSION);

//set global date formatter. this is valid
$twig->getEnvironment()
    ->getExtension(\Twig\Extension\CoreExtension::class)
    ->setDateFormat("F jS \\a\\t g:ia");


$app->add(TwigMiddleware::create($app, $twig));



$app->get('/echo/{msg}', [ new EchoAction(), 'watch'] );

$app->get('/forbidden', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    // echo '<pre>';
    // var_dump($request);
    // echo '</pre>';
    // die();
    
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_internal', function (Request $request, Response $response) {
    return $response->withHeader('Location','/forbidden');
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    $articles = DB::query(
        "SELECT a.id AS 'id', a.title AS 'title', a.body AS 'body',
                a.created_at AS 'createdAt', u.username AS 'authorName' 
            FROM articles AS a
            JOIN users AS u
            ON u.id = a.authorId
            ORDER BY a.created_at"
    );
    
    return $view->render($response, 'index.html.twig', [
        'articles' => $articles
    ]);
});

$app->get('/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    return $view->render($response, 'login.html.twig');
});

$app->post('/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $loginInfo = $request->getParsedBody();

    if ($loginInfo != null) {
        if (isset($loginInfo['username']) && isset($loginInfo['password'])) {
            $user = DB::queryFirstRow("SELECT * FROM users WHERE username = %s", $loginInfo['username']);
            if ($loginInfo['password'] === $user['password']) {
                unset($user['password']);
                $_SESSION['user'] = $user;

                return $response
                    ->withHeader('Location', '/');
            }
        }
    }

    return $view->render($response, 'login.html.twig', [
        'error' => "Username doesn't match password."
    ]);
});

$app->get('/logout', function (Request $request, Response $response) {
    unset($_SESSION['user']);
    return $response->withHeader('Location', '/');
});

$app->get('/register', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});

$app->post('/register', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $registerInfo = $request->getParsedBody();

    $username = $registerInfo['username'];
    $email = $registerInfo['email'];
    $password = $registerInfo['password'];
    $errors = [];

    if (strlen($username) < 6 || strlen($username) > 20) {
        $errors['username'] = "Username must be 6~20 chars";
        $registerInfo['username'] = '';
    } elseif (isUsernameTaken($username)) {
        $errors['username'] = "User is already exist.";
        $username = '';
    }

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $errors['email'] = "Invalid Email";
        $email = '';
    }

    if (strlen($password) < 6 || strlen($password) > 100
        || preg_match("/[a-z]/", $password) == false
        || preg_match("/[A-Z]/", $password) == false
        || preg_match("/[0-9#$%^&*()+=-\[\]';,.\/{}|:<>?~]/", $password) == false) {
        $errors['password'] = "Password must be 6~100 characters,
                            must contain at least one uppercase letter, 
                            one lower case letter, 
                            and one number or special character.";
    } elseif ($password !== $_POST['confirm']) {
        $errors['password'] = "Passwords must be same.";
    }

    if (empty($errors)) {
        DB::insert('users', [
            'username' => $username,
            'email' => $email,
            'password' => $password
        ]);
        $user = DB::affectedRows()[0];

        unset($user['password']);

        $_SESSION['user'] = $user;

        return $response->withHeader('Location', '/');
    }

    return $view->render($response, 'register.html.twig', [
        'errors' => $errors,
        'prevInput' => [
            'username' => $username,
            'email' => $email
        ]
    ]);
});

$app->get('/article/add', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (!isset($_SESSION['user'])) {
        //TODO : show user a message says "must login first"
        return $response->withHeader('Location', '/login');
    }

    return $view->render($response, "articleadd.html.twig");
});

$app->post('/article/add', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (!isset($_SESSION['user'])) {
        //TODO : show user massege days "Must login first"
        return $response->withHeader('Location', '/login');
    }

    $articleInfo = $request->getParsedBody();
    $title = $articleInfo['title'];
    $body = $articleInfo['body'];
    $errors = [];

    if (strlen($title) < 10) {
        $errors['title'] = "Article title must be at least 10 characters long.";
    }

    //Sanitize the body
    //1) only allow certain HTML tags, 2) make sure it is valid HTMl
    $body = strip_tags($body, "<p><ul><li><em><strong><i><b><ol><h3><h4><span>");

    if (strlen($body) < 50) {
        $errors['body'] = "Article content must be at least 50 characters long.";
    }
    
    if (empty($errors)) {
        DB::insert('articles', [
            'title' => $title,
            'body' => $body,
            'authorId' => $_SESSION['user']['id']
        ]);
        return $response->withHeader('Location', '/');
    }

    return $view->render($response, 'articleadd.html.twig', [
        'errors' => $errors,
        'prevInput' => [
            'title' => $title,
            'body' => $body
        ]
    ]);
});

$app->get('/article/{id:[0-9]+}', function (Request $request, Response $response, array $args) use ($log) {
    $view = Twig::fromRequest($request);
    $articleId = $args['id'];

    if( !isValidArticleId($articleId)){
        $log->notice(sprintf('Invalid Post Attempt: [/article/%d]User try to add comment on invalid article', $articleId));
        return $response->withHeader('Location', '/error_internal', 403);
        // TODO : create new page for this
    }

    $article = DB::queryFirstRow(
        "SELECT a.id AS 'id', a.title AS 'title', a.body AS 'body',
            a.created_at AS 'createdAt', u.username AS 'authorName'
       FROM articles AS a
       JOIN users AS u
       ON u.id = a.authorId
       WHERE a.id = %i",
        $args['id']
    );

    $comments = DB::query(
        "SELECT c.body AS 'body', c.created_at AS 'createdAt',
                u.username AS 'authorName'
        FROM comments AS c
        JOIN users AS u
        ON c.authorId = u.id
        WHERE c.articleId = %i
        ORDER BY c.created_at DESC",
        $args['id']
    );

    return $view->render($response, "article_view.html.twig", [
       'article' => $article,
       'comments' => $comments,
       'prevInput' => [
           'commentBody' => $_GET['commentBody']?? ''
       ],
       'error' => $_GET['error'] ?? ''
   ]);
});

$app->post('/article/{id:[0-9]+}/comment/add', function (Request $request, Response $response, array $args) use ($log) {

    $articleId = $args['id'];

    if (!isset($_SESSION['user'])) {
        $log->notice(sprintf('Invalid Post Attempt: [/article/%d]Guest User try to add comment', $articleId));
        return $response->withHeader('Location', '/error_internal', 403);
        // TODO : create new page for this
    }

    if( !isValidArticleId($articleId)){
        $log->notice(sprintf('Invalid Post Attempt: [/article/%d]User try to add comment on invalid article', $articleId));
        return $response->withHeader('Location', '/error_internal', 403);
        // TODO : create new page for this
    }

    $comment = $request->getParsedBody();
    $redirectUrl = '/article/' . $articleId;
    
    if(strlen($comment['commentBody']) < 5 || strlen($comment['commentBody']) > 5000){
        $redirectUrl .= '?error=' . htmlspecialchars('Comment must be 5~5000 characters.')
                        . '&commentBody=' . $comment['commentBody'];
    } else {
        DB::insert('comments', [
            'body' => $comment['commentBody'],
            'authorId' => $_SESSION['user']['id'],
            'articleId' => $articleId
        ]);
    }

    return $response->withHeader('Location',$redirectUrl);
});


$app->get('/register/isusernametaken/[{username}]', function (Request $request, Response $response, array $args){
    $error = '';

    if(isset($args['username'])){
        $error = isUsernameTaken($args['username']) ? "It's already taken." :'';
    }

    $response->getBody()->write($error);
    return $response;
});

$app->run();

function isValidArticleId($articleId)
{
    $result = DB::queryFirstRow("SELECT id FROM articles WHERE id=%i",$articleId);
    return !empty($result);
}

function isUsernameTaken($username)
{
    $result = DB::queryFirstRow("SELECT COUNT(*) AS 'userCount' FROM users WHERE username = %s", $username);

    if ($result['userCount'] == 0) {
        return false;
    } elseif ($result['userCount'] == 1) {
        return true;
    } else {
        echo "Internal Error: duplicate username.";//FIXME : Log instead of echoing
        return true;
    }
}
