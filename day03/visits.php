<?php

    session_start();

    // Purpose of this script is to display the number of visits to this
    // page made from current browser (browsing session)

    if (!isset($_SESSION['visitCounter'])) {
        $_SESSION['visitCounter'] = 0;
    }

    $_SESSION['visitCounter']++;

    echo "Visit counter is : " . $_SESSION['visitCounter'];

?>