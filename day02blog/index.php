<?php
    require_once "db.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>

    <!-- layout(header, section) css -->
    <link rel="stylesheet" type="text/css" href="styles/layout.css" />
    <!-- index css -->
    <link rel="stylesheet" type="text/css" href="styles/index.css" />
</head>
<body>
    <?php
        include "templetes/header.php";
    ?>

    <section>
        <h1>Welcome to my blog, read on!</h1>

        <?php
            $articles = getALLArticles($conn);

            if(empty($articles)){
                echo "<p>There is no article. <a href='/register.php'>Add one now</a></p>";
            }
            else {

                echo "<ul>";

                foreach ($articles as $article) {
                    $id = $article['id'];
                    $title = $article['title'];
                    $authorName = $article['authorName'];
                    $createdAt = $article['createdAt'];
                    $body = $article['body'];

                    if (strlen($body) > 200) {
                        $body = substr($body, 0, 200) . ' ... ';
                    }

                    echo "<li>
                            <h2><a href='/article.php?id=$id'>$title</a></h2>
                            <small>Posted by $authorName on $createdAt</small>
                            <p>$body</p>
                        </li>";
                }

                echo "</ul>";
            }
        ?>
    </section>
</body>
</html>