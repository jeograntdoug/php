<?php
    require_once "db.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Add Article</title>

    <!-- layout(header, section) css -->
    <link rel="stylesheet" type="text/css" href="styles/layout.css" />
    <!-- articleadd css -->
    <link rel="stylesheet" type="text/css" href="styles/articleadd.css" />
</head>
<body>
    <?php
        include "templetes/header.php";
    ?>

    <section>
<?php
    function printForm($title,$body)
    {
        $form =
<<<ENDFORM
    <h2>Craete New Article</h2>
    <form method="POST">
        <div>
            <label>Title</label>
            <input type="text" name="title" value="$title"/>
        </div>
        <div>
            <label>content</label>
            <textarea cols=30 rows=10 name="body">$body</textarea>
        </div>
        <div class="buttonContainer">
            <button type="submit">Add Article</button>
        </div>
    </form>
ENDFORM;
        echo $form;
    }
    $title = '';
    $body = '';

    if(!isset($_SESSION['user'])){
        echo "<h2>Must be log in to add article. Wanna <a href='/login.php'>Login?</a>";
        echo " or <a href='/'>go home?</a> or <a href='/articles'> see articles?</a></h2>";
        exit;
    }

    if(isset($_POST['title'])){
        $title = $_POST['title'];
        $body = $_POST['body'];
        $errors = [];

        if(strlen($title) < 10){
            $errors['title'] = "Article title must be at least 10 characters long.";
        }

        //Sanitize the body
        //1) only allow certain HTML tags, 2) make sure it is valid HTMl
        $body = strip_tags($body, "<p><ul><li><em><strong><i><b><ol><h3><h4><span>");

        if(strlen($body) < 50){
            $errors['body'] = "Article content must be at least 50 characters long.";
        }
        
        if(empty($errors)){
            addArticle($title,$body,$conn);// add article into database

            echo "<h2>Article added! wanna go <a href='/'>Home?</a>";
            echo " or <a href='/articles.php'> see Articles?</a></h2>";
            exit;
        }

        echo "<ul>";
        foreach($errors as $error){
            echo "<li>$error</li>\n";
        }
        echo "</ul>";
    }

    printForm($title,$body);
?>
    </section>
</body>

</html>