<?php

    session_start();

    try {
        $user = 'day02blog';
        $pass = 'R^QrN2}Db5w><:n';
        $conn = new PDO("mysql:host=localhost:3333;dbname=day02blog", $user, $pass);
    } catch (Exception $e) {
        echo "Cannot connect to database: " . $e->getMessage();
        exit();
    }

    function isUsernameTaken($username, $conn)
    {
        $stmt = $conn->prepare("SELECT * FROM users WHERE username= :username");
        $stmt->bindParam(':username', $username);
        $result = $stmt->execute();

        if (!$result) {
            echo "SQL Query failed: ". $stmt->errorInfo();
            exit;
        }

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        return $user??false;
    }

    function isValidUser($username, $password, $conn)
    {
        $stmt = $conn->prepare("SELECT * FROM users WHERE username = :username");
        $stmt->bindParam(':username', $username);
        $result = $stmt->execute();

        if (!$result) {
            echo "SQL Query failed: ". $stmt->erorrInfo();
            exit;
        }

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if (empty($user)) {
            return false;
        } elseif ($user['password'] !== $password) {
            return false;
        }

        return $user;
    }

    function addArticle($title, $body, $conn)
    {
        $stmt = $conn->prepare("INSERT INTO articles (title,body,authorId) "
         . " VALUES(:title,:body,:authorId)");
        $stmt->bindParam(":title", $title);
        $stmt->bindParam(":body", $body);
        $stmt->bindParam(":authorId", $_SESSION['user']['id']);
        $result = $stmt->execute();

        if (!$result) {
            echo "SQL Query failed: ". $stmt->errorInfo();
            exit;
        }
    }

    function getArticle($articleId, $conn)
    {
        $stmt = $conn->prepare(
            "SELECT a.title AS 'title', a.body AS 'body'"
            . ", a.created_at AS 'createdAt' , u.username AS 'authorName' "
            . " FROM articles AS a "
            . " JOIN users AS u "
            . " ON a.authorId = u.id "
            . " WHERE a.id = :id"
        );

        $stmt->bindParam(':id', $articleId);
        $result = $stmt->execute();

        if (!$result) {
            echo "SQL Query failed: " . $stmt->errorInfo();
            exit;
        }

        $article = $stmt->fetch(PDO::FETCH_ASSOC);

        return $article?? false;
    }

    function getALLArticlesByUserId($userId, $conn)
    {
        $stmt = $conn->prepare("SELECT * FROM articles WHERE authorId = :authorId");
        $stmt->bindParam(':authorId', $userId);
        $result = $stmt->execute();

        if (!$result) {
            echo "SQL Query failed: " . $stmt->errorInfo();
            exit;
        }

        return $stmt->fetchAll();
    }

    function getALLArticles($conn)
    {
        $stmt = $conn->prepare(
            "SELECT a.id AS 'id', a.title AS 'title', a.body AS 'body',
                a.created_at AS 'createdAt', u.username AS 'authorName' 
            FROM articles AS a
            JOIN users AS u
            ON u.id = a.authorId"
        );
        $result = $stmt->execute();

        if (!$result) {
            echo "SQL Query failed: " . $stmt->errorInfo();
            exit;
        }

        return $stmt->fetchAll();
    }

    function getALLCommentsByArticleId($articleId, $conn)
    {
        $stmt = $conn->prepare(
            "SELECT c.body AS 'body', c.created_at AS 'createdAt' 
                    ,u.username AS 'authorName'
            FROM comments AS c 
            JOIN users AS u 
            ON u.id = c.authorId
            WHERE articleId = :articleId"
        );
        $stmt->bindParam(':articleId', $articleId);
        $result = $stmt->execute();

        if (!$result) {
            echo "SQL Query failed: " . $stmt->errorInfo();
            exit;
        }
       
        return $stmt->fetchAll();
    }

    function addComment($articleId, $authorId, $body, $conn)
    {
        $stmt = $conn->prepare("INSERT INTO comments (articleId,authorId,body) "
            . " VALUES(:articleId, :authorId, :body)");
        $stmt->bindParam(':articleId', $articleId);
        $stmt->bindParam(':authorId', $authorId);
        $stmt->bindParam(':body', $body);
        $result = $stmt->execute();

        if (!$result) {
            echo "SQL Query failed: " . $stmt->errorInfo();
            exit;
        }
    }
