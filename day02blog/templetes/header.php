    <header>
        <div>
            <?php
                if(isset($_SESSION['user'])){
                    echo "<span>You are logged in as " . $_SESSION['user']['username'] . ".</span>"
                        . "<a href='/logout.php'>Logout</a>";
                } else{
                    echo "<a href='/login.php'>Login</a> or <a href='/register.php'>Register</a>";
                }
            ?>
        </div>
    </header>