<?php
    require_once "db.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
</head>
<body>
<?php
    function printForm()
    {
        $form =
<<<ENDFORM
    <form method="POST">
        <div>
            <label>Username</label>
            <input type="text" name="username" />
        </div>
        <div>
            <label>Password</label>
            <input type="password" name="password" />
        </div>
        <div>
            <button type="submit">Login</button>
        </div>
        <div>
            <a href="./register.php">No account? Register here</a>
        </div>

    </form>
ENDFORM;

        echo $form;
    }

    if(isset($_POST['username'])){
        $username = $_POST['username'];
        $password = $_POST['password'];
        $isValid = true;

        
        if (strlen($username) < 6 || strlen($username) > 20) {
            $isValid = false;
        }

        if(strlen($password) < 6 || strlen($password) > 100
            || preg_match("/[a-z]/",$password) == false
            || preg_match("/[A-Z]/",$password) == false
            || preg_match("/[0-9#$%^&*()+=-\[\]';,.\/{}|:<>?~]/",$password) == false)
        {
            $isValid = false;
        }

        if($isValid){
            $user = isValidUser($username,$password,$conn);
            if($user){
                unset($user['password']);
                $_SESSION['user'] = $user;

                echo "Login succeed!. Wanna go to <a href='/'>home?</a>";
                exit;
            }
            $isValid = false;
        }

        if(!$isValid){
            echo "<p>Username doesn't match with password</p>";
        }
    }
    printForm();
?>
</body>
</html>