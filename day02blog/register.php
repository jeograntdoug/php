<?php
    require_once "db.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $('input[name=username').keyup(function() {
                var username = $('input[name=username').val();
                $('#isTaken').load("isusernametaken.php?username=" + username);
            });

            $(document).ajaxError(function(event, jqxhr, settings, throwError){
                console.log("Ajax error occured on " + settings.url);
            });
        })
    </script>

    <title>Document</title>
</head>
<body>
    
<?php

function printForm($username='',$email=''){
    $form = <<<ENDFORM
<h2>New User registration</h2>

<form method="POST" >
    <div>
        <label>Desired username</label>
        <input type="text" name="username" />
        <span class="errorMessage" id="isTaken"></span>
    </div>
    <div>
        <label>your email</label>
        <input type="text" name="email" />
    </div>
    <div>
        <label>Password</label>
        <input type="password" name="password" />
    </div>
    <div>
        <label>Password(repeat)</label>
        <input type="password" name="confirm" />
    </div>
    <div>
        <button type="submit">Register!</button>
        <a href="/">Go Home</a>
    </div>
</form>
ENDFORM;
    
    echo $form;
}

    $username = '';
    $email = '';

    if(isset($_SESSION['user'])){
        echo "<h2>Hey," .  $_SESSION['user']['username'] . "You are already log in.";
        echo "wanna go <a href='/'>Home?</a> or see <a href='/article.php'>Articles?</a></h2>";
        exit;
    }
    if(isset($_POST['username'])){
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $errors = [];

        if(strlen($username) < 6 || strlen($username) > 20){
            $errors['username'] = "Username must be 6~20 chars";
            $username = '';
        } else if(isUsernameTaken($username,$conn)){
            $errors['username'] = "User is already exist.";
            $username = '';
        }

        if(filter_var($email, FILTER_VALIDATE_EMAIL) == false){
            $errors['email'] = "Invalid Email";
            $email = '';
        }

        if(strlen($password) < 6 || strlen($password) > 100
            || preg_match("/[a-z]/",$password) == false
            || preg_match("/[A-Z]/",$password) == false
            || preg_match("/[0-9#$%^&*()+=-\[\]';,.\/{}|:<>?~]/",$password) == false)
        {
            $errors['password'] = 
<<<PASSERROR
Password must be 6~100 characters,
must contain at least one uppercase letter, 
one lower case letter, 
and one number or special character.
PASSERROR;

        } else if($password !== $_POST['confirm']){
            $errors['password'] = "Passwords must be same.";
        }

        if(empty($errors)){
            $stmt = $conn->prepare("INSERT INTO users (username, email, password)" .
                " VALUES (:username,:email,:password)");
            $stmt->bindParam(':username', $username);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':password', $password);
            $result = $stmt->execute();

            if(!$result){
                echo "INTERNAL ERROR: Cannot insert data";
                exit;
            }

            echo "<p>Registration successful</p>";
            echo '<p><a href="/login.php">Click here to login</a></p>';
            exit;
        }

        echo "<ul>";
        foreach($errors as $error){
            echo "<li>$error</li>";
        }
        echo "</ul>";
    }
    printForm($username,$email);
?>
</body>
</html>