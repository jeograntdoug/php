<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Logout</title>
</head>
<body>
    <?php
        unset($_SESSION['user']);
        echo "Session: ";
        var_dump($_SESSION);
        echo "<div>logout!, wanna <a href='/login.php'>login?</a></div>";
    ?>
</body>
</html>