<?php
    require_once "db.php";
?>

<?php

function printArticle($article){
    $title = $article['title'];
    $authorName = $article['authorName'];
    $createdAt = $article['createdAt'];
    $body = $article['body'];

    $articleFormat = 
<<<ENDARTICLE
        <article class="article">
            <h2>$title</h2>
            <p>Posted by $authorName on $createdAt</p>
            <div>$body</div>
        </article>
ENDARTICLE;
    echo $articleFormat;
}

function printForm($body = ''){
    $form =
<<<ENDFORM
            <form method="POST" class="commentForm">
                <div>
                    <p>My comment:</p>
                    <button type="submit">Add comment</button>
                </div>
                <textarea cols=30 rows=3 name="commentBody">$body</textarea>
            </form>
ENDFORM;

    if(isset($_SESSION['user'])){
        echo "<article>
                $form
            </article>";
    } else {
        echo "<article>
                <h3>You must <a href='/login.php'>log in</a>
                or <a href='/register.php'>register</a> to post comments</h2>
            </article>";
    }
}

function printComments($comments){
    if(!empty($comments)){
        echo "<article class='previousComents'>
                <h3>Previous comments:</h2>";

        foreach($comments as $comment){
            echo "<div><p>" . $comment['authorName'] 
                ." said on ". $comment['createdAt'] . "</p>"
                ."<p>" . $comment['body'] . "</p></div>";
        }

        echo "</article>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Articles</title>

    <!-- layout(header, section) css -->
    <link rel="stylesheet" type="text/css" href="styles/layout.css" />
    <!-- article css -->
    <link rel="stylesheet" type="text/css" href="styles/article.css" />
</head>
<body>
    <?php
        include "templetes/header.php";
    ?>

    <section>
<?php
    $article = [];
    $comments = [];
    $error = '';
    $commentBody = '';

    if(!isset($_GET['id'])){
        echo "<h2>Please select valid article ID</h2>";
        exit;
    }

    $articleId = $_GET['id'];

    $article = getArticle($articleId,$conn);

    if($article == false){
        echo "<h2>Please select valid article ID</h2>";
        exit;
    }

    printArticle($article);

    if(isset($_POST['commentBody'])){
        $commentBody= $_POST['commentBody'];
        $authorId = $_SESSION['user']['id'];

        if(strlen($commentBody) < 5 || strlen($commentBody) > 5000){
            $error = "Comment must be 5~5000 characters.";
        } else {
            addComment($articleId,$authorId,$commentBody,$conn);
            $commentBody = '';
        }
        //TODO: Error Message;
    }

    printForm($commentBody);

    $comments = getALLCommentsByArticleId($articleId, $conn);
    printComments($comments);

?>


    </section>
</body>
</html>