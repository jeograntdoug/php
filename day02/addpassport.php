<?php

    include "./db.php";

?>

<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset="UTP-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
	<title>Add Person</title>

</head>
<body>

<?php
    $passNo = '';
    $passPhoto = '';
    $errors = [];

    if (isset($_POST['passNo'])) {
        $passNo = $_POST['passNo'];
        $passPhoto = $_FILES['passPhoto'];
        $extensions = ['jpeg','jpg','png'];

        if (!preg_match('/^[A-Z]{2}[0-9]{8}$/', $passNo)) {
            $errors['passNo'] = 'PassNo must be AB12345678 format';
        }

        $photoExt = strtolower(end(explode('.', $passPhoto['name'])));//FIXME: type value

        if (!in_array($photoExt, $extensions)) {
            $errors['passPhoto']
                = "Passport photo must be 'jpeg','jpg','png' type";
        } else {
            list($width, $height) = getimagesize($passPhoto['tmp_name']);

            if ($width < 200 || $width > 1000
                ||$height < 200 || $height > 1000) {
                $errors['passPhoto']
                    = "Passport photo must be 200~1000 width and height";
            }
        }

        //TODO: validate photo;
        
        if (empty($errors)) {
            $photoPath = "uploads/".$passNo. '.' . $photoExt;

            $stmt = $conn->prepare("INSERT INTO day02passport (passNo,photoPath) Values (:passNo, :photoPath)");

            $stmt->bindParam(':passNo', $passNo);
            $stmt->bindParam(':photoPath', $photoPath);

            $stmt->execute();
            var_dump($passNo, $photoPath);
            die();

            move_uploaded_file($passPhoto['tmp_name'], $photoPath);
            echo 'Upload succeed!';
        }
    }
?>

<ul>
<?php
    foreach ($errors as $error) {
        echo "<li>$error</li>";
    }
?>
</ul>

<form method="POST" enctype = "multipart/form-data">
	<div>
		<label>Pass No.</label>
		<input type="text" name="passNo" 
			value="<?php echo isset($errors['passNo'])?'':$passNo; ?>"/>
	</div>
	<div>
		<input type="file" name="passPhoto" >
	</div>
	<div>
		<input type="submit" value="Upload" />
	</div>
</form>

</body>
</html>

